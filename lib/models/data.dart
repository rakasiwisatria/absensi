// class Data {
//   int? id;
//   String? nama;
//   String? username;
//   String? lasactive;
//   List<Program>? program;

//   Data({this.id, this.nama, this.username, this.lasactive, this.program});

//   Data.fromJson(Map<String, dynamic> json) {
//     id = json['id'];
//     nama = json['nama'];
//     username = json['username'];
//     lasactive = json['lasactive'];
//     if (json['program'] != null) {
//       program = <Program>[];
//       json['program'].forEach((v) {
//         program!.add(new Program.fromJson(v));
//       });
//     }
//   }

//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['id'] = this.id;
//     data['nama'] = this.nama;
//     data['username'] = this.username;
//     data['lasactive'] = this.lasactive;
//     if (this.program != null) {
//       data['program'] = this.program!.map((v) => v.toJson()).toList();
//     }
//     return data;
//   }
// }

// class Program {
//   String? kode;
//   String? nama;
//   List<Riwayat>? riwayat;

//   Program({this.kode, this.nama, this.riwayat});

//   Program.fromJson(Map<String, dynamic> json) {
//     kode = json['kode'];
//     nama = json['Nama'];
//     if (json['riwayat'] != null) {
//       riwayat = <Riwayat>[];
//       json['riwayat'].forEach((v) {
//         riwayat!.add(new Riwayat.fromJson(v));
//       });
//     }
//   }

//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['kode'] = this.kode;
//     data['Nama'] = this.nama;
//     if (this.riwayat != null) {
//       data['riwayat'] = this.riwayat!.map((v) => v.toJson()).toList();
//     }
//     return data;
//   }
// }

// class Riwayat {
//   String? materi;
//   String? tgl;
//   List<Mhs>? mhs;

//   Riwayat({this.materi, this.tgl, this.mhs});

//   Riwayat.fromJson(Map<String, dynamic> json) {
//     materi = json['materi'];
//     tgl = json['tgl'];
//     if (json['mhs'] != null) {
//       mhs = <Mhs>[];
//       json['mhs'].forEach((v) {
//         mhs!.add(new Mhs.fromJson(v));
//       });
//     }
//   }

//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['materi'] = this.materi;
//     data['tgl'] = this.tgl;
//     if (this.mhs != null) {
//       data['mhs'] = this.mhs!.map((v) => v.toJson()).toList();
//     }
//     return data;
//   }
// }

// class Mhs {
//   String? id;
//   String? nama;
//   String? status;
//   String? waktumasuk;

//   Mhs({this.id, this.nama, this.status, this.waktumasuk});

//   Mhs.fromJson(Map<String, dynamic> json) {
//     id = json['id'];
//     nama = json['nama'];
//     status = json['status'];
//     waktumasuk = json['waktumasuk'];
//   }

//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['id'] = this.id;
//     data['nama'] = this.nama;
//     data['status'] = this.status;
//     data['waktumasuk'] = this.waktumasuk;
//     return data;
//   }
// }

import 'dart:convert';

List<Post> postFromJson(String str) =>
    List<Post>.from(json.decode(str).map((x) => Post.fromMap(x)));

class Post {
  Post({
    required this.id,
    required this.title,
    required this.body,
  });

  int id;
  String title;
  String body;

  factory Post.fromMap(Map<String, dynamic> json) => Post(
        id: json["id"],
        title: json["name"],
        body: json["description"],
      );
}
