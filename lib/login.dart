import 'package:absensi/home.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 120.0),
              child: Center(
                child: Container(
                    width: 200,
                    height: 150,
                    child: Text(
                      'Selemat Datang',
                      style:
                          TextStyle(fontWeight: FontWeight.w600, fontSize: 40),
                    )),
              ),
            ),
            Padding(
              //padding: const EdgeInsets.only(left:15.0,right: 15.0,top:0,bottom: 0),
              padding: EdgeInsets.symmetric(horizontal: 15),
              child: TextFormField(
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'username',
                    hintText: 'usernamae'),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  left: 15.0, right: 15.0, top: 15, bottom: 0),
              //padding: EdgeInsets.symmetric(horizontal: 15),
              child: TextFormField(
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Password',
                    hintText: 'Enter secure password'),
              ),
            ),
            SizedBox(
              height: 90,
            ),
            Container(
              height: 50,
              width: 250,
              decoration: BoxDecoration(
                  color: Colors.blue, borderRadius: BorderRadius.circular(20)),
              child: TextButton(
                onPressed: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (_) => Homepage()));
                },
                child: Text(
                  'Login',
                  style: TextStyle(color: Colors.white, fontSize: 25),
                ),
              ),
            ),
            SizedBox(
              height: 130,
            ),
          ],
        ),
      ),
    );
  }

  // void _validateInputs() {
  //   if (_formKey.currentState!.validate()) {
  //     //If all data are correct then save data to out variables
  //     _formKey.currentState!.save();

  //     doLogin(txtEditEmail.text, txtEditPwd.text);
  //   }
  // }

  // doLogin(username, password) async {
  //   final GlobalKey<State> _keyLoader = GlobalKey<State>();

  //   try {
  //     final response = await http.post(Uri.parse('https://reqres.in/api/login'),
  //         body: {'email': 'eve.holt@reqres.in', 'password': 'cityslicka'});

  //     final output = jsonDecode(response.body.toString());
  //     if (response.statusCode == 200) {
  //       Navigator.of(_keyLoader.currentContext!, rootNavigator: false).pop();
  //       ScaffoldMessenger.of(context).showSnackBar(
  //         SnackBar(
  //             content: Text(
  //           output['token'],
  //           style: const TextStyle(fontSize: 16),
  //         )),
  //       );

  //       if (output['token'] == true) {
  //         saveSession(username);
  //       }
  //       //debugPrint(output['message']);
  //     } else {
  //       Navigator.of(_keyLoader.currentContext!, rootNavigator: false).pop();
  //       //debugPrint(output['message']);
  //       ScaffoldMessenger.of(context).showSnackBar(
  //         SnackBar(
  //             content: Text(
  //           output.toString(),
  //           style: const TextStyle(fontSize: 16),
  //         )),
  //       );
  //     }
  //   } catch (e) {
  //     Navigator.of(_keyLoader.currentContext!, rootNavigator: false).pop();

  //     debugPrint('$e');
  //   }
  // }

  // saveSession(String username) async {
  //   SharedPreferences pref = await SharedPreferences.getInstance();
  //   await pref.setString("username", username);
  //   await pref.setBool("is_login", true);

  //   Navigator.pushAndRemoveUntil(
  //     context,
  //     MaterialPageRoute(
  //       builder: (BuildContext context) => const Homepage(),
  //     ),
  //     (route) => false,
  //   );
  // }

  // void ceckLogin() async {
  //   SharedPreferences pref = await SharedPreferences.getInstance();
  //   var islogin = pref.getBool("is_login");
  //   if (islogin != null && islogin) {
  //     Navigator.pushAndRemoveUntil(
  //       context,
  //       MaterialPageRoute(
  //         builder: (BuildContext context) => const Homepage(),
  //       ),
  //       (route) => false,
  //     );
  //   }
  // }
}
