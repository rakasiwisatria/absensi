import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';

class RiwayatAbsen extends StatefulWidget {
  const RiwayatAbsen({super.key});

  @override
  State<RiwayatAbsen> createState() => _RiwayatAbsenState();
}

class _RiwayatAbsenState extends State<RiwayatAbsen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            'Riwayat Absen',
          ),
          elevation: 0.0,
        ),
        body: ListView(
          children: [
            riwayat(materi: "KONTRAK KULIAH", tanggal: "29.01.100"),
            riwayat(materi: "KONTRAK KULIAH", tanggal: "29.01.100")
          ],
        ));
  }
}

class absensi extends StatelessWidget {
  String nama;
  String waktu;
  absensi({super.key, required this.nama, required this.waktu});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            child: Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                        color: Colors.amber,
                        borderRadius: BorderRadius.circular(4.0)),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 8.0, vertical: 4.0),
                      child: Row(
                        children: <Widget>[Text(nama)],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 8.0,
                  ),
                  Text(
                    'ABSEN : ${waktu}',
                    style: TextStyle(fontWeight: FontWeight.w600),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class riwayat extends StatelessWidget {
  String materi;
  String tanggal;
  riwayat({super.key, required this.materi, required this.tanggal});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 30.0),
      child: Column(
        children: <Widget>[
          SizedBox(height: 20.0),
          ExpansionTile(
            title: Text(
              materi,
              style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
            ),
            subtitle: Text(
              tanggal,
              style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
            ),
            children: <Widget>[
              absensi(nama: "MUHAMMAR GHANI", waktu: "15.00"),
              absensi(nama: "MUHAMMAR GHANI", waktu: "15.00"),
            ],
          ),
        ],
      ),
    );
  }
}
